/**
 * @author Christian Geiger
 */

const bundler = require('./util/bundler')
const fs = require('fs-extra')
const path = require('path')
const comServer = require('./communication/server')
const nextUid = require('./util/uid-generator')
const pageTemplate = require('./util/page-template')
const htmlWrapper = require('./util/html-wrapper')

function createTagFrom (name) {
  return (name[0].toUpperCase() + name.slice(1)).replace(' ', '_').replace(/\./g, '-').replace(/[^a-zA-Z0-9-_:]/g, '')
}

module.exports = function (RED) {
  /*
   * TODOs:
   * ------
   *
   * - Ensure filenames are valid (no spaces or special characters, first character uppercase)
   * - Make use of Node-RED's new done-method
   * - Take better care of file extension (add only if missing)
   * - Check if route already exists
   *
   */

  function WebPageNode (config) {
    /*
     * Global variables and consts
     */

    const openRequests = new Map() // Open requests that get currently processed by Node-RED

    /*
     * Initialize configuration
     */

    RED.nodes.createNode(this, config)

    // Initialize node
    const node = this
    const simplifiedId = node.id.replace(/\./g, '')
    node.app = RED.nodes.getNode(config.app)
    node.route = (config.route[0] === '/' ? '' : node.app.baseroute) + config.route
    node.requestReceivedHook = config.requestReceivedHook
    node.cachingAllowed = config.cachingAllowed
    node.layout = config.layout ? config.layout : node.app.layout
    node.layoutTag = node.layout
    if (node.layoutTag.indexOf('/') > -1) {
      node.layoutTag = node.layoutTag.substring(node.layoutTag.lastIndexOf('/') + 1)
    }
    node.layoutTag = createTagFrom(node.layoutTag)
    node.slots = config.slots
    node.actions = config.actions
    node.homeDir = RED.settings.userDir
    node.projectHome = RED.settings.userDir
    const projects = RED.settings.get('projects')
    if (projects) {
      node.projectHome = path.join(node.projectHome, 'projects', projects.activeProject)
    }
    node.tag = 'Page_' + simplifiedId
    node.filename = path.join('~', 'pages', node.tag + '.svelte')
    node.component = node.filename.replace(/\\/g, '/')
    node.lastChange = config.lastChange

    // Initialize statistics
    const flowContext = this.context().flow
    if (!flowContext.get('web_requests')) {
      flowContext.set('web_requests', {})
    }
    if (!flowContext.get('web_requests')[node.route]) {
      flowContext.get('web_requests')[node.route] = {
        ssr: 0, // A counter of all page requests to be shown as the node's status
        pages: 0, // A counter of all page requests to be shown as the node's status
        data: 0, // A counter of all data requests to be shown as the node's status
        actions: 0 // A counter of all action requests to be shown as the node's status
      }
    }
    const requests = this.context().flow.get('web_requests')[node.route]

    // Initialize build-directory
    let buildFolder = path.join(node.projectHome, 'pekfinger-red', 'build')
    let buildFolderExists = fs.existsSync(buildFolder)
    if (!buildFolderExists) {
      const error = fs.mkdirSync(buildFolder)
      if (error) {
        node.error({ message: 'Could not initialize build folder: ' + error.message, ...error })
        return
      }
    }
    buildFolder = path.join(buildFolder, node.tag)
    buildFolderExists = fs.existsSync(buildFolder)
    if (!buildFolderExists) {
      const error = fs.mkdirSync(buildFolder)
      if (error) {
        node.error({ message: 'Could not initialize build folder: ' + error.message, ...error })
      }
    }

    // Check if the page template already exists and whether a rebuild is needed
    const filename = path.join(buildFolder, node.tag + '.svelte')
    let rebuildNeeded = true
    if (fs.existsSync(filename)) {
      const fileStats = fs.statSync(filename)
      if (node.lastChange && fileStats.mtime.getTime() > node.lastChange) {
        rebuildNeeded = false
      }
    }

    // Prepare page template
    const pageDescription = {
      name: node.tag,
      layout: node.layoutTag,
      slots: [],
      files: {
        [node.layoutTag]: path.join(node.projectHome, 'pekfinger-red', 'layouts', node.layout + '.svelte')
      }
    }
    pageDescription.head = node.app.readHeadForPwa(node.projectHome)
    let slotNumber = 0
    node.slots.forEach(function (slot) {
      slotNumber += 1
      const tag = (slot.type === 'component' ? slot.component : 'Code_' + slotNumber)

      // Add to files
      if (!Object.prototype.hasOwnProperty.call(pageDescription.files, tag)) {
        if (slot.type === 'component') {
          pageDescription.files[tag] = path.join(node.projectHome, 'pekfinger-red', 'components', tag + '.svelte')
        } else {
          pageDescription.files[tag] = path.join(buildFolder, tag + '.svelte')
        }
      }

      // Write code to the build directory
      if (rebuildNeeded && slot.type === 'code') {
        fs.writeFileSync(pageDescription.files[tag], slot.code, 'UTF-8')
      }

      // Add to slots
      pageDescription.slots.push({
        tag,
        id: slotNumber,
        name: slot.name || 'default',
        selector: slot.data.replace(/'/g, "\\'")
      })
    })

    // Initialize bundler
    bundler.init({
      debug: node.debug,
      warn: node.warn,
      error: node.error,
      dev: node.app.developmentMode,
      homeDir: node.homeDir,
      projectHome: node.projectHome,
      buildFolder,
      id: node.tag,
      file: filename,
      lastChange: node.lastChange,
      files: [filename, ...Object.values(pageDescription.files)]
    })

    // Create page template
    if (rebuildNeeded) {
      node.debug('Writing page template ' + filename + ' to disk.')
      const code = pageTemplate(pageDescription)
      fs.writeFileSync(filename, code, 'UTF-8')
    }

    // Watch page template
    let css
    bundler.watch(node.tag, (domBundle) => {
      css = undefined
      domBundle = `
        ${domBundle}
        new ${node.tag}.default({
          target: document.querySelector('#pekfinger-red-content'),
          hydrate: true,
          props: {
              client: client,
              server: server,
              jsonata: jsonata
          }
        })
      `
      node.gateway.getAllSessions().forEach((session) => {
        // Update all clients that already visited this page / route
        if (session.routes.includes(node.route)) {
          node.debug('Updating client-bundle for session ' + session.id + ' and route ' + node.route + '.')
          node.gateway.setPage(session.id, domBundle, {
            route: node.route,
            cachingAllowed: node.cachingAllowed
          })
        }
      })
    })

    /*
     * Gateway for client-server-communication
     */

    node.registerAtGateway = function () {
      // Init socket.io and add handler for several events
      node.gateway = comServer.init(node.app)
      node.gateway.register(node, {
        afterRegister () {
          // Force (reconnect and) reload
          const sessions = Array.from(node.gateway.getOldSessions())
          sessions.forEach(session => {
            if (session.routes.includes(node.route)) {
              node.gateway.reconnectAndReload(session.id)
            }
          })
          node.gateway.clearOldSessions()
        },
        getBundle (sessionId, route, sessionData, requestedBefore) {
          requests.pages += 1
          node.updateNodeStatus()
          if (node.requestReceivedHook && !requestedBefore) {
            const msg = {
              _type: 'bundle',
              _session_id: sessionId,
              session_data: sessionData,
              request: {
                url: route.url,
                parameters: route.parameters
              },
              payload: {},
              _return_to: node.id
            }
            node.send([msg])
          } else {
            node.getContents().then(async function (bundle) {
              node.gateway.setPage(sessionId, bundle.dom, {
                route: route.route,
                url: route.url,
                cachingAllowed: node.cachingAllowed
              })
            })
          }
        },
        getData (sessionId, route, sessionData) {
          requests.data += 1
          node.updateNodeStatus()
          if (node.requestReceivedHook) {
            const msg = {
              _type: 'data',
              _session_id: sessionId,
              session_data: sessionData,
              request: {
                url: route.url,
                parameters: route.parameters
              },
              payload: {},
              _return_to: node.id
            }
            node.send([msg])
          } else {
            // Default: empty data object
            node.gateway.setData(sessionId, {}, { url: route.url, cachingAllowed: true })
          }
        },
        trigger (actionRequest, sessionData) {
          requests.actions += 1
          node.updateNodeStatus()
          const msg = {
            _session_id: actionRequest.sessionId,
            _type: 'action',
            options: actionRequest.action.options,
            parameters: {
              ...(typeof actionRequest.action.options === 'object' ? actionRequest.action.options : {}),
              ...(typeof actionRequest.action.data === 'object' ? actionRequest.action.data : { payload: actionRequest.action.data })
            },
            payload: actionRequest.action.data,
            _return_to: node.id,
            session_data: sessionData
          }
          const result = node.requestReceivedHook ? [null] : []
          node.actions.forEach(function (action) {
            if (action === actionRequest.action.type) {
              result.push(msg)
            } else {
              result.push(null)
            }
          })
          node.send(result)
        },
        isCachingAllowed () {
          return node.cachingAllowed
        }
      })
    }

    /* A helper function for updating the status */
    node.updateNodeStatus = function (initial = false) {
      node.status({
        fill: initial ? 'grey' : 'red',
        shape: initial ? 'ring' : 'dot',
        text: 'Requests: ' + requests.ssr + ' / ' + requests.pages + ' / ' + requests.data + ' / ' + requests.actions + (openRequests.size > 0 ? ' (' + openRequests.size + ' open)' : '')
      })
      if (!initial) {
        setTimeout(() => {
          node.status({
            fill: 'grey',
            shape: 'dot',
            text: 'Requests: ' + requests.ssr + ' / ' + requests.pages + ' / ' + requests.data + ' / ' + requests.actions + (openRequests.size > 0 ? ' (' + openRequests.size + ' open)' : '')
          })
        }, 500)
      }
    }
    node.updateNodeStatus(true)

    /*
     * Content rendering
     */

    node.getContents = function () {
      return new Promise(resolve => {
        bundler.getBundle(node.tag).then((bundle) => {
          // Wrap bundle
          bundle = htmlWrapper(bundle, {
            id: node.tag,
            dev: node.app.developmentMode,
            defaultLang: node.defaultLanguage,
            version: node.app.version,
            timestamp: node.app.timestamp,
            cachingAllowed: node.cachingAllowed,
            debug: node.debug,
            warn: node.warn,
            error: node.error
          })
          // Serve CSS
          node.app.app.get('/' + node.tag + '/bundle.css', function (request, response) {
            if (!css) css = fs.readFileSync(path.join(buildFolder, 'bundle.css'), 'utf-8')
            response.type('text/css').send(css)
          })
          if (node.app.developmentMode) {
            node.app.app.get('/' + node.tag + '/bundle.css.map', function (request, response) {
              const cssMap = fs.readFileSync(path.join(buildFolder, 'bundle.css.map'), 'utf-8')
              response.type('text/css').send(cssMap)
            })
          }
          resolve(bundle)
        })
      })
    }

    /*
     * Event handling (client-node tells us what to do next)
     */

    const event = 'web:' + node.id
    const handler = function (msg) {
      // Update session data
      if (msg.session_data && msg._session_id) node.gateway.updateSessionData(msg._session_id, msg.session_data)

      if (msg._type === 'bundle') {
        // This is a bundle request
        node.getContents().then(async function (bundle) {
          let domBundle = bundle.dom
          if (msg.lang) {
            domBundle = "document.documentElement.lang = '" + msg.lang + "'\n" + domBundle
          } else if (node.app.defaultLanguage) {
            domBundle = "document.documentElement.lang = '" + node.app.defaultLanguage + "'\n" + domBundle
          }
          node.gateway.setPage(msg._session_id, domBundle, {
            route: node.route,
            url: msg.request.url,
            cachingAllowed: node.cachingAllowed,
            data: msg.payload
          })
        })
      } else if (msg._type === 'data') {
        // This is a data request
        node.gateway.setData(msg._session_id, msg.payload, {
          url: msg.request.url,
          cachingAllowed: node.cachingAllowed
        })
      } else if (msg._type === 'request') {
        // Error handling if no requestId is set or the id is not known
        if (!msg._request_id) {
          node.error('No request-id set. Does the msg really stem from a page-node?')
          return
        }
        const response = openRequests.get(msg._request_id)
        if (!response) {
          node.error('Request ' + msg._request_id + ' could not be found. Maybe a timeout?')
          return
        }

        // This request is done
        openRequests.delete(msg._request_id)
        node.updateNodeStatus()

        if (msg._return_type === 'send') {
          // Create a session and render the page
          const session = node.gateway.createSession(node.route)
          if (msg.session_data) session.sessionData = msg.session_data
          msg._session_id = session
          node.getContents().then(async function (bundle) {
            const html = await bundle.createSSR(msg)
            response.setHeader('Caching-Allowed', node.cachingAllowed)
            response.status(msg._status_code || 200).send(html)
          })
        } else if (msg._return_type === 'end') {
          // End immediately
          response.status(msg._status_code || 404).end()
        } else if (msg._return_type === 'redirect') {
          // Redirect
          response.status(msg._status_code || 303).redirect(msg._redirect_to)
        } else if (msg._action) {
          node.error('Action ' + msg._action + ' cannot be called before page has been loaded.')
        } else {
          node.error("Unknown return type '" + msg._return_type + "'.")
        }
      } else if (msg._type === 'action') {
        if (msg._return_type === 'action') {
          // Call the client-action
          node.gateway.action(msg._session_id, msg._action, msg.payload, msg.options)
        } else if (msg._return_type === 'send') {
          // Send msg.payload as new data
          node.gateway.setData(msg._session_id, msg.payload, msg.options)
        } else if (msg._return_type === 'end') {
          // End immediately
          // Caution: Return type 'end' will be ignored as this flow originated from an action. It is just meant as a visual statement.
        } else if (msg._return_type === 'redirect') {
          // Redirect
          if (msg._redirect_to.match(/^[[:word:]]+:\/\/.*/)) {
            // This redirects to an external source
            node.gateway.redirectTo(msg._session_id, msg._redirect_to, {})
          } else {
            // This redirects to an internal path
            let route = msg._redirect_to
            if (route.startsWith('./')) {
              route = node.route + route.substring(1)
            } else if (!route.startsWith('/')) {
              route = node.app.baseroute + '/' + route
            }
            if (node.gateway.find(msg._redirect_to)) {
              node.gateway.redirectTo(msg._session_id, route, {
                data: msg.payload,
                cachingAllowed: node.gateway.find(msg._redirect_to).handler.isCachingAllowed
              })
            } else {
              node.error('Cannot redirect to: ' + msg._redirect_to + '. No route found.')
            }
          }
        } else {
          node.error("Unknown return type '" + msg._return_type + "'.")
        }
      }
    }
    RED.events.on(event, handler)

    // Clean up event handler on close
    node.on('close', function () {
      RED.events.removeListener(event, handler)
      bundler.unwatch(node.tag, { debug: node.debug })
      node.gateway.remove(node.route)
    })

    /*
     * Serve content
     */

    node.app.app.get(node.route, function (req, res) {
      // Create session
      const requestId = nextUid('request')
      openRequests.set(requestId, res)
      requests.ssr += 1
      // Update node-status
      node.updateNodeStatus()
      // The request needs to be cleaned up in case it gets not requested after at the defined timeout (if javascript is disabled or the connection is disrupted)
      setTimeout(function () {
        if (openRequests.has(requestId)) {
          node.warn('Request ' + requestId + ' timed out. It will be forcefully ended now.')
          openRequests
            .get(requestId)
            .status(504)
            .end()
          openRequests.delete(requestId)
          node.updateNodeStatus()
        }
      }, node.app.timeout)
      const msg = {
        _request_id: requestId,
        _type: 'request',
        request: {
          url: req.url,
          parameters: req.params,
          query: req.query
        },
        parameters: { ...req.params, ...req.query },
        session_data: {},
        payload: {}
      }
      // Is there anything we have to do before we render the page? (get data from a database, access control, ...)
      if (node.requestReceivedHook) {
        requests.data += 1
        msg._return_to = node.id
        node.send([msg])
      } else {
        msg._return_type = 'send'
        handler(msg)
      }
    })

    // Tell the app that we are ready
    node.app.registerPage(this)
  }

  RED.nodes.registerType('web-page', WebPageNode)
}
