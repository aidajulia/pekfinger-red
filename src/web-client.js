/**
 * @author Christian Geiger
 */

const jsonata = require('jsonata')

module.exports = function (RED) {
  function WebClientNode (config) {
    RED.nodes.createNode(this, config)
    const node = this
    node.returnType = config.returnType
    node.url = config.url
    node.statusCode = config.statusCode
    if (node.returnType === 'action') {
      node.actionType = config.actionType
      node.actionOptions = jsonata(config.actionOptions)
      node.continueAfterAction = config.continueAfterAction
    }
    node.sendPayload = config.sendPayload

    /* A helper function for updating the status */
    let responses = 0
    node.updateNodeStatus = function (initial = false) {
      if (!initial) {
        responses += 1
        setTimeout(() => {
          node.status({
            fill: 'grey',
            shape: 'dot',
            text: 'Count: ' + responses
          })
        }, 500)
      }
      node.status({
        fill: initial ? 'grey' : 'red',
        shape: initial ? 'ring' : 'dot',
        text: 'Count: ' + responses
      })
    }
    node.updateNodeStatus(true)

    node.on('input', function (msg) {
      if (!msg._return_to) {
        this.error('Missing meta data for returning to page-node.')
      } else {
        node.updateNodeStatus()
        const orgMsg = RED.util.cloneMessage(msg)
        msg._return_type = node.returnType
        if (node.returnType === 'redirect') {
          if (!node.sendPayload) {
            msg.payload = undefined
          }
          msg._redirect_to = node.url
          msg._redirect_to = msg._redirect_to.replace(/{\w*}/g, function (name) {
            return msg.parameters[name.slice(1, -1)] || name
          })
          msg._status_code = node.statusCode
        } else if (node.returnType === 'action') {
          msg._action = node.actionType
          msg.options = node.actionOptions.evaluate(msg)
        } else if (node.returnType === 'end') {
          msg._status_code = node.statusCode
        }
        const event = 'web:' + msg._return_to
        msg._event = event
        RED.events.emit(event, msg)
        if (node.continueAfterAction) node.send(orgMsg)
      }
    })
  }
  RED.nodes.registerType('web-client', WebClientNode)
}
