/* eslint-disable no-undef */

/*
 * We register a service worker for better PWA support and for caching
 */
if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/serviceworker.js').then(function (registration) {
      // Nothing to do here
    }, function (error) {
      // Registration failed
      console.log('Service worker could not be registered: ', error)
    })
  })
}

/*
 * We store page bundles in either a Map or an indexDB for caching
 */
class Storage {
  constructor (name) {
    this.name = name
    this.internalMap = new Map()
    function createHashCode (str) {
      let hash = 0
      if (str.length === 0) return hash
      for (let i = 0; i < str.length; i++) {
        const char = str.charCodeAt(i)
        hash = ((hash << 5) - hash) + char
        hash = hash & hash
      }
      if (hash < 0) hash = -hash
      return hash
    }

    if (window.indexedDB) {
      const request = window.indexedDB.open('pekfinger-red-' + name, createHashCode(version))
      request.onupgradeneeded = (event) => {
        this.internalDatabase = request.result
        if (event.oldVersion === 0) {
          const objectStore = this.internalDatabase.createObjectStore(name, { keyPath: 'key' })
          objectStore.createIndex('key', 'key', { unique: true })
        } else {
          request.transaction.objectStore(name).clear()
        }
      }
      request.onsuccess = () => {
        this.internalDatabase = request.result
        this.dbReady = true
        this.initializeIfNecessary()
      }
    }
  }

  promisify (request) {
    return new Promise((resolve, reject) => {
      request.onsuccess = () => {
        resolve(request.result)
      }
      request.onerror = (error) => {
        reject(error)
      }
    })
  }

  async initializeIfNecessary () {
    // Move all entries that have been added during initialization of the db
    if (this.internalMap.size > 0) {
      const tempMap = this.internalMap
      this.internalMap = new Map()
      const objectStore = this.internalDatabase.transaction([this.name], 'readwrite').objectStore(this.name)
      for (const key of tempMap.keys()) {
        const value = tempMap.get(key)
        objectStore.add({ key, value })
      }
      objectStore.onsuccess = () => {
        this.initialized = true
      }
    }
    // If this is the first rendering and we have a cached version open it (offline rendering)
    if (client.isFirstRender && await this.has(client.currentUrl) && this.name === 'pages') {
      client.open(client.currentUrl)
    }
  }

  async get (key) {
    if (this.dbReady) {
      const result = await this.promisify(this.internalDatabase.transaction([this.name], 'readonly').objectStore(this.name).index('key').get(key))
      return result ? result.value : undefined
    } else {
      return this.internalMap.get(key)
    }
  }

  async set (key, value) {
    if (this.dbReady) {
      await this.promisify(this.internalDatabase.transaction([this.name], 'readwrite').objectStore(this.name).put({ key, value }))
    } else {
      this.internalMap.set(key, value)
    }
  }

  async has (key) {
    if (this.dbReady) {
      try {
        const result = await this.promisify(this.internalDatabase.transaction([this.name], 'readonly').objectStore(this.name).index('key').get(key))
        if (result && result.value) {
          return true
        } else {
          return false
        }
      } catch (e) {
        return false
      }
    } else {
      return this.internalMap.has(key)
    }
  }

  async keys () {
    if (this.dbReady) {
      const result = await this.promisify(this.internalDatabase.transaction([this.name], 'readonly').objectStore(this.name).index('key').getAllKeys())
      return result
    } else {
      return Array.from(this.internalMap.keys())
    }
  }

  async clear () {
    const database = await this.promisify(window.indexedDB.open('pekfinger-red-' + this.name, 1))
    await this.promisify(database.transaction([this.name], 'readwrite').objectStore(this.name).clear())
  }
}

/*
 * An object representing the client
 */
export const client = {
  title: 'App',
  data,
  connected: false,
  actions: new Map(),
  actionsGlobal: new Map(),
  volatilePageCache: new Map(),
  pageCache: new Storage('pages'),
  dataCache: new Storage('data'),
  isFirstRender: true,
  eventHandler: new Map(),
  eventHandlerOnce: new Map(),
  eventHandlerGlobal: new Map(),
  currentUrl,
  nextUrl: currentUrl,
  nextPage: undefined,
  nextData: data,
  define (action, callback, global = false) {
    if (global) {
      this.actionsGlobal.set(action, callback)
    } else {
      if (this.actions.has(action)) {
        const callbacks = this.actions.get(action)
        callbacks.push(callback)
      } else {
        this.actions.set(action, [callback])
      }
    }
  },
  defineGlobal (action, callback) {
    this.define(action, callback, true)
  },
  getDataFor (url) {
    return this.dataCache.get(url)
  },
  select (selector) {
    return jsonata(selector).evaluate({
      msg: {
        payload: data
      }
    })
  },
  run (action, data, options) {
    if (action && (this.actions.has(action) || this.actionsGlobal.has(action))) {
      if (this.actions.has(action)) {
        this.actions.get(action).forEach(function (actionHandler) {
          actionHandler(data, options)
        })
      }
      if (this.actionsGlobal.has(action)) {
        this.actions.get(action)(data, options)
      }
    } else {
      const error = new Error('Unknown action: ' + action)
      error.data = data
      error.options = options
      throw error
    }
  },
  async open (url, data) {
    const continueExecution = client.notify('client.open.before', { url, data })
    if (!continueExecution) return

    // Handle non-relative URLs
    if (url.match(/^[[:word:]]+:\/\/.*/)) {
      window.open(url)
    }

    // Handle relative URLs
    if (this.nextUrl && url !== this.nextUrl) {
      // The request for a new page might come while a previous request is still handled, so we need to reset some things
      this.nextPage = undefined
      this.nextData = undefined
    }
    this.nextUrl = url

    const numberOfCachedPages = this.isFirstRender ? 0 : (await this.pageCache.keys()).length + this.volatilePageCache.size
    this.isFirstRender = false
    const pageNeeded = this.currentUrl !== url || numberOfCachedPages === 0
    const dataNeeded = typeof this.nextData === 'undefined'

    // Get page code
    if (pageNeeded) {
      if (this.volatilePageCache.has(url)) {
        this.nextPage = this.volatilePageCache.get(url)
      } else if (await this.pageCache.has(url)) {
        this.nextPage = await this.pageCache.get(url)
      } else {
        // Search page
        const routes = [...Array.from(await this.pageCache.keys()), ...Array.from(await this.volatilePageCache.keys())]
        const parts = url.split('/')
        const route = routes.find(toCheck => {
          const checkParts = toCheck.split('/')
          if (checkParts.length === parts.length) {
            let found = true
            for (let i = 0; i < parts.length; i++) {
              if (checkParts[i][0] !== ':' && checkParts[i] !== parts[i]) {
                found = false
              }
            }
            return found
          }
          return false
        })
        if (route) {
          this.nextPage = this.volatilePageCache.has(route) ? this.volatilePageCache.get(route) : await this.pageCache.get(route)
        } else {
          // Request page
          server.loadPageFor(url)
        }
      }
    }

    if (dataNeeded) {
      // Get data
      if (data) {
        // The data is given to us
        this.nextData = data
      } else if (await this.dataCache.has(url)) {
        // We have data for this route in the cache so we can directly show the page
        this.nextData = await this.dataCache.get(url)
      } else {
        // We have to request the data from the server
        this.nextData = 'pending...' // Just a marker so that the data doesn't get requested twice
        server.getDataFor(url)
      }
    }

    // Show page if everything is there
    if (typeof this.nextPage !== 'undefined' && typeof this.nextData !== 'undefined' && this.nextData !== 'pending...') {
      this.show(this.nextUrl, this.nextPage, this.nextData)
      this.nextUrl = undefined
      this.nextPage = undefined
      this.nextData = undefined
    }
    client.notify('client.open')
    client.notify('client.open.after')
  },
  show (url, pageCode, pageData) {
    const continueExecution = client.notify('client.show.before', { url, pageCode, pageData, show: this.showInternal.bind(this) })
    if (continueExecution) {
      this.showInternal(url, pageCode, pageData)
    }
  },
  showInternal (url, pageCode, pageData) {
    const addHistory = url !== this.currentUrl
    this.currentUrl = url
    this.data = pageData
    this.eventHandler.clear()
    this.actions.clear()
    new Function('data', 'client', 'server', pageCode)(client.data, client, server) // eslint-disable-line no-new-func
    window.scrollTo(0, 0) // scroll to top
    const state = {
      url
    }
    if (addHistory) {
      window.history.pushState(state, window.title, url)
    } else {
      window.history.replaceState(state, window.title, url)
    }
    client.notify('client.show')
    client.notify('client.show.after')
  },
  async isCurrentRoute (route) {
    const urlParts = client.currentUrl.split('/')
    const routeParts = route.split('/')
    if (urlParts.length === routeParts.length) {
      for (let i = 0; i < routeParts.length; i++) {
        if (routeParts[i][0] !== ':' && routeParts[i] !== urlParts[i]) {
          return false
        }
      }
      return true
    }
    return false
  },
  on (event, callback, global = false) {
    let handlerForEvent
    const handlerMap = global ? this.eventHandlerGlobal : this.eventHandler
    if (handlerMap.has(event)) {
      handlerForEvent = handlerMap.get(event)
    } else {
      handlerForEvent = []
      handlerMap.set(event, handlerForEvent)
    }
    handlerForEvent.push(callback)
  },
  once (event, callback) {
    let handlerForEvent
    if (this.eventHandlerOnce.has(event)) {
      handlerForEvent = this.eventHandlerOnce.get(event)
    } else {
      handlerForEvent = []
      this.eventHandlerOnce.set(event, handlerForEvent)
    }
    handlerForEvent.push(callback)
  },
  notify (event, data) {
    let cancelled = false
    const onceHandler = this.eventHandlerOnce.get(event)
    if (onceHandler) {
      Array.from(onceHandler).forEach((handler) => {
        if (!cancelled) {
          const continueExecution = handler(data)
          if (typeof continueExecution === 'boolean' && !continueExecution) cancelled = true
        }
      })
      this.eventHandlerOnce.delete(event)
      if (cancelled) return false
    }
    const eventHandler = this.eventHandler.get(event)
    if (eventHandler) {
      Array.from(eventHandler).forEach((handler) => {
        if (!cancelled) {
          const continueExecution = handler(data)
          if (typeof continueExecution === 'boolean' && !continueExecution) cancelled = true
        }
      })
      if (cancelled) return false
    }
    const eventHandlerGlobal = this.eventHandlerGlobal.get(event)
    if (eventHandlerGlobal) {
      Array.from(eventHandlerGlobal).forEach((handler) => {
        if (!cancelled) {
          const continueExecution = handler(data)
          if (typeof continueExecution === 'boolean' && !continueExecution) cancelled = true
        }
      })
      if (cancelled) return false
    }
    return true
  },
  deferred (callback, delay) {
    const runAt = Date.now() + delay
    function runDeferred () {
      const currentMillis = Date.now()
      if (currentMillis >= runAt) {
        callback()
      } else {
        requestAnimationFrame(runDeferred)
      }
    }
    requestAnimationFrame(runDeferred)
  }
}

/*
 * Ensure that the back button works as supposed
 */

window.onpopstate = async function (event) {
  const state = event.state
  if (!state) return
  if (state.url && await client.pageCache.has(state.url) && await client.dataCache.has(state.url)) {
    // Show state if caching is allowed
    client.currentUrl = state.url
    const pageCode = await client.pageCache.get(state.url)
    client.data = await client.dataCache.get(state.url)
    new Function('data', 'client', 'server', pageCode)(client.data, client, server) // eslint-disable-line no-new-func
  } else {
    // Show alert if no event handler has been registered
    if (!(client.eventHandler.has('history.back') || client.eventHandlerOnce.has('history.back'))) {
      alert('The page cannot be shown. Please refresh your browser.')
    } else {
      // Notify any event handlers
      client.notify('history.back')
    }
  }
}

/*
 *  Some basic actions
 */
client.define('alert', function (data, options) {
  if (options && options.message) {
    alert(options.message)
  } else {
    alert(data)
  }
})

/*
 * An object for server-interaction
 */
export const server = {
  socket: undefined,
  sessionId,
  run (actionName, data, options) {
    server.socket.emit('action', {
      sessionId: server.sessionId,
      url: (options && options.url) ? options.url : client.currentUrl,
      action: {
        type: actionName,
        data,
        options
      }
    })
  },
  connect () {
    // Open connection to server
    server.socket = io()

    /*
     * Socket.io stuff
     */

    // Update status on connect ...
    server.socket.on('connect', () => {
      client.connected = true
      client.notify('server.connected')
    })

    // ... and disconnect
    server.socket.on('disconnect', () => {
      client.connected = false
      client.notify('server.disconnected')
    })

    // The server wants to get our session ID
    server.socket.on('getSessionId', function () {
      server.socket.emit('setSessionId', { sessionId: server.sessionId, url: client.currentUrl })
      // If we do not have the bundle yet, we need to request it
      client.open(client.currentUrl)
    })

    // We get a new session
    server.socket.on('setSessionId', function (id) {
      server.sessionId = id
      // Notify any event handlers
      client.notify('server.setSessionId', id)
    })

    // We need to reconnect as the old gateway has ended
    server.socket.on('reconnect', function () {
      // Notify any event handlers
      const continueExecution = client.notify('server.reconnect.before')
      if (continueExecution) {
        server.socket.close()
        server.connect()
        // Notify any event handlers
        client.notify('server.reconnect.after')
        client.notify('server.reconnect')
      }
    })

    // We get redirected to another URL
    server.socket.on('open', async function (url, options) {
      if (options.data && options.cachingAllowed) {
        await client.dataCache.set(url, options.data)
      }
      client.open(url, options.data)
    })

    // The server gives us a specific page
    server.socket.on('setPage', async function (pageCode, options) {
      // Notify any event handlers
      const continueExecution = client.notify('server.setPage.before', { pageCode, options })
      if (continueExecution) {
        if (options.data) {
          if (options.cachingAllowed) {
            await client.dataCache.set(options.route, options.data)
          }
          if (client.currentUrl === options.url) {
            client.data = options.data
          }
        }
        if (!options.cachingAllowed && (!await client.volatilePageCache.has(options.route) || await client.volatilePageCache.get(options.route) !== pageCode)) {
          client.volatilePageCache.set(options.route, pageCode)
        } else if (options.cachingAllowed && (!await client.pageCache.has(options.route) || await client.pageCache.get(options.route) !== pageCode)) {
          await client.pageCache.set(options.route, pageCode)
        }
        // Try to show the page
        if (client.nextUrl && options.url === client.nextUrl) {
          client.nextPage = pageCode
          client.open(client.nextUrl, options.data)
        } else if (!options.url && !options.data && client.isCurrentRoute(options.route)) {
          // This is an in place code replacement
          // 1st: Reload CSS
          const links = document.getElementsByTagName('link')
          for (const link of links) {
            if (link.rel === 'stylesheet') {
              link.href += ''
            }
          }
          // 2nd: Rerender page
          client.showInternal(client.currentUrl, pageCode, client.data)
        }
        // Notify any event handlers
        client.notify('server.setPage.after', { pageCode, options })
        client.notify('server.setPage', { pageCode, options })
      }
    })

    // The server wants us to reconnect and reload the current page
    server.socket.on('reconnectAndReload', function () {
      client.volatilePageCache = new Map()
      client.pageCache = new Storage('pages')
      // A reconnect is needed as the new content will get deployed through a new socketio-connection (the old one is obsolete)
      server.socket.close()
      server.connect()
      client.on('server.setSessionId', () => {
        client.open(client.currentUrl, client.data)
        // Notify any event handlers
        client.notify('server.reconnectAndReload')
      }, true)
    })

    // The server sets new data
    server.socket.on('setData', async function (newData, options) {
      // Notify any event handlers
      client.notify('server.setData.before', { newData, options })
      // Cache data if allowed
      if (options && options.url && options.cachingAllowed) {
        await client.dataCache.set(options.url, newData)
      }
      // Immediately update the data if it is for the current URL
      if (client.currentUrl === options.url) {
        client.data = newData
        data = client.data
      }
      // Try to show the page
      if (typeof client.nextUrl !== 'undefined' && options.url === client.nextUrl) {
        client.nextData = newData
        client.open(client.nextUrl, newData)
      }
      // Notify any event handlers
      client.notify('server.setData.after', { newData, options })
      client.notify('server.setData', { newData, options })
    })

    // Seems our session has expired
    server.socket.on('sessionInvalid', function () {
      // Notify any event handlers
      const continueExecution = client.notify('server.sessionInvalid')
      if (continueExecution) server.socket.emit('requestNewSessionId', { url: client.currentUrl })
    })

    // The server triggers an action
    server.socket.on('action', function (action) {
      // Notify any event handlers
      client.notify('server.action.before', action)
      // Run action
      client.run(action.type, action.data, action.options)
      // Notify any event handlers
      client.notify('server.action.after', action)
      client.notify('server.action', action)
    })
  },
  getDataFor (url) {
    server.socket.emit('getDataFor', {
      sessionId: server.sessionId,
      url
    })
  },
  loadPageFor (url) {
    if (typeof url === 'string') {
      server.socket.emit('loadPageFor', {
        sessionId: server.sessionId,
        url
      })
    } else if (typeof url === 'object' && url.constructor === 'Array') {
      url.forEach((singleUrl) => {
        server.socket.emit('loadPageFor', {
          sessionId: server.sessionId,
          url: singleUrl
        })
      })
    }
  }
}

// Initial connection
server.connect()
