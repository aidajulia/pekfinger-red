const io = require('socket.io')
const nextUid = require('../util/uid-generator')

const connections = new Map()
const oldSessions = new Map()

class Gateway {
  constructor (socketio, app) {
    this.closed = false
    this.socketio = socketio
    this.handlers = new Map()
    this.sessions = {}
    this.app = app

    const gateway = this

    if (app.scope === 'global') {
      app.context().global.set('web_sessions.' + app.tag, this.sessions)
    } else {
      app.context().flow('web_sessions.' + app.tag, this.sessions)
    }

    socketio.on('connection', function (socket) {
      if (app.developmentMode) {
        console.log('[pekfinger-red] communication server: connection (' + app.name + ')')
      }

      let sessionId

      socket.emit('getSessionId')

      // The client sends us its session-ID
      socket.on('setSessionId', function (request) {
        if (app.developmentMode) {
          console.log('[pekfinger-red] communication server: setSessionId (' + app.name + ')', request)
        }

        // Yes, the client has a session-ID.
        sessionId = request.sessionId
        // Let's try to get the session.
        if (!gateway.hasSession(sessionId)) {
          // Seems the session has timed out or is otherwise invalid
          if (app.developmentMode) {
            console.log('[pekfinger-red] communication server: setSessionId, session invalid (' + app.name + ')', request)
          }
          socket.emit('sessionInvalid')
        } else {
          const session = gateway.getSession(sessionId)
          session.socket = socket
        }
      })

      // The client needs a new session (usually because of a previous disconnect and session timeout)
      socket.on('requestNewSessionId', function (request) {
        if (app.developmentMode) {
          console.log('[pekfinger-red] communication server: requestNewSessionId (' + app.name + ')', request)
        }

        const route = gateway.find(request.url)
        if (route) {
          sessionId = gateway.createSession(route.route)
          const session = gateway.getSession(sessionId)
          session.socket = socket
          socket.emit('setSessionId', sessionId)
        } else {
          console.log('[pekfinger-red] communication server: Critical exception: Could not find route for URL "' + request.url + '". Will ignore it (client may break).')
        }
      })

      // The client wants to get a page bundle
      socket.on('loadPageFor', function (request) {
        if (app.developmentMode) {
          console.log('[pekfinger-red] communication server: loadPageFor (' + app.name + ')', request)
        }

        gateway.execute(request, socket, function (route, session) {
          const requestedBefore = session.routes.includes(route.route)
          if (!requestedBefore) {
            session.routes.push(route.route)
          }
          route.handler.getBundle(session.id, route, session.data, requestedBefore)
        })
      })

      // The client wants to get the data for a specific URL
      socket.on('getDataFor', function (request) {
        if (app.developmentMode) {
          console.log('[pekfinger-red] communication server: getDataFor (' + app.name + ')', request)
        }

        gateway.execute(request, socket, function (route, session) {
          route.handler.getData(sessionId, route, session.data)
        })
      })

      // The client has disconnected. This can be temporarily as the connection might be broken.
      socket.on('disconnect', function () {
        if (app.developmentMode) {
          console.log('[pekfinger-red] communication server: disconnect (' + app.name + ')')
        }

        // Set timeout to remove sessions in case the connection doesn't come up again
        if (gateway.hasSession(sessionId)) {
          const session = gateway.getSession(sessionId)
          session.lastContact = Date.now()
          if (app.sessionTTL > 0) {
            setTimeout(function () {
              if (gateway.hasSession(sessionId)) {
                const session = gateway.getSession(sessionId)
                if (session.lastContact <= Date.now() - app.sessionTTL) {
                  gateway.removeSession(session.id)
                }
              }
            }, app.sessionTTL)
          }
        }
      })

      // The client triggers an action
      socket.on('action', function (request) {
        if (app.developmentMode) {
          console.log('[pekfinger-red] communication server: action (' + app.name + ')', request)
        }

        gateway.execute(request, socket, function (route, session) {
          route.handler.trigger(
            request,
            session.data
          )
        })
      })
    })
  }

  register (node, handler) {
    handler.node = node
    this.handlers.set(node.route, handler)
    handler.afterRegister()
  }

  remove (route) {
    this.handlers.delete(route)
  }

  createSession (route) {
    const id = nextUid('session')
    const session = {
      id,
      data: {},
      socket: null,
      lastContact: Date.now(),
      routes: [route]
    }
    this.sessions[id] = session
    return id
  }

  removeSession (id) {
    delete this.sessions[id]
  }

  hasSession (id) {
    return this.sessions.hasOwnProperty(id) // eslint-disable-line no-prototype-builtins
  }

  getSession (id) {
    return this.sessions[id]
  }

  getAllSessions () {
    return Object.values(this.sessions)
  }

  updateSessionData (sessionId, data) {
    this.getSession(sessionId).data = data
  }

  find (url) {
    const routes = Array.from(this.handlers.keys())
    const parts = url.split('/')
    let parameters
    const route = routes.find(toCheck => {
      if (toCheck === url) {
        return true
      } else {
        parameters = {}
        const checkParts = toCheck.split('/')
        if (checkParts.length === parts.length) {
          let found = true
          for (let i = 0; i < parts.length; i++) {
            if (checkParts[i][0] !== ':' && checkParts[i] !== parts[i]) {
              found = false
            } else if (checkParts[i][0] === ':') {
              parameters[checkParts[i].substring(1)] = parts[i]
            }
          }
          return found
        }
        return false
      }
    })
    if (route) {
      return {
        url,
        route,
        handler: this.handlers.get(route),
        parameters
      }
    } else return null
  }

  execute (request, socket, callback) {
    if (this.hasSession(request.sessionId)) {
      const session = this.getSession(request.sessionId)
      session.lastContact = Date.now()
      const routeData = this.find(request.url)
      if (routeData) {
        callback(routeData, session)
      } else if (this.closed) {
        socket.emit('reconnect')
      } else {
        this.app.error('No route for ' + request.url)
      }
    } else {
      socket.emit('session_invalid')
    }
  }

  action (sessionId, type, data, options) {
    // The server wants to trigger an action on the client
    const socket = this.getSession(sessionId).socket
    if (socket) {
      socket.emit('action', {
        type,
        data,
        options
      })
    }
  }

  setPage (sessionId, bundle, options) {
    const socket = this.getSession(sessionId).socket
    if (socket) socket.emit('setPage', bundle, options)
  }

  setData (sessionId, data, options) {
    const socket = this.getSession(sessionId).socket
    if (socket) socket.emit('setData', data, options)
  }

  reconnectAndReload (sessionId) {
    if (this.hasSession(sessionId)) {
      const socket = this.getSession(sessionId).socket
      if (socket) socket.emit('reconnectAndReload')
    } else if (oldSessions.has(sessionId)) {
      const socket = oldSessions.get(sessionId).socket
      if (socket) socket.emit('reconnectAndReload')
    }
  }

  redirectTo (sessionId, url, options) {
    const socket = this.getSession(sessionId).socket
    if (socket) socket.emit('open', url, options)
  }

  getOldSessions () {
    return oldSessions.values()
  }

  clearOldSessions () {
    oldSessions.clear()
  }
}

module.exports = {
  init (app) {
    if (!connections.has(app.id)) {
      const socketio = io(app.server)
      const gateway = new Gateway(socketio, app)
      connections.set(app.id, gateway)
      return gateway
    } else {
      return connections.get(app.id)
    }
  },
  reset (app) {
    if (connections.has(app.id)) {
      const gateway = connections.get(app.id)
      gateway.closed = true
      gateway.getAllSessions().forEach((session) => oldSessions.set(session.id, session))
      connections.delete(app.id)
    }
  }
}
