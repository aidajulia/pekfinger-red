/**
 * @author Christian Geiger
 */

const fs = require('fs')
const path = require('path')
const nextUid = require('./util/uid-generator')
const mimeTypes = require('./util/mimetypes')

module.exports = function (RED) {
  /*
   * TODOs:
   * ------
   *
   * - Implement caching?
   * - Add a hook for manipulating the content
   * - Add access to (global) modules
   * - Add support for buffers / streams
   *
   */

  function WebContentNode (config) {
    /*
     * Global variables and consts
     */

    const openRequests = new Map() // Open requests that get currently processed by Node-RED

    /*
     * Initialize configuration
     */

    RED.nodes.createNode(this, config)

    // Initialize node
    const node = this
    node.app = RED.nodes.getNode(config.app)
    node.contentType = config.contentType
    node.route = (config.route[0] === '/' ? '' : node.app.baseroute) + config.route
    node.content = config.content
    node.requestReceivedHook = config.requestReceivedHook
    node.cachingAllowed = config.cachingAllowed
    node.homeDir = RED.settings.userDir
    node.projectHome = path.join(node.homeDir, config.projectHome)

    // Initialize statistics
    const flowContext = this.context().flow
    if (!flowContext.get('web_requests')) {
      flowContext.set('web_requests', {})
    }
    if (!flowContext.get('web_requests')[node.route]) {
      flowContext.get('web_requests')[node.route] = {
        content: 0 // A counter of all requests to be shown as the node's status
      }
    }
    const requests = this.context().flow.get('web_requests')[node.route]

    /* A helper function for updating the status */
    node.updateNodeStatus = function (initial = false) {
      node.status({
        fill: initial ? 'grey' : 'red',
        shape: initial ? 'ring' : 'dot',
        text: 'Requests: ' + requests.content + (openRequests.size > 0 ? ' (' + openRequests.size + ' open)' : '')
      })
      if (!initial) {
        setTimeout(() => {
          node.status({
            fill: 'grey',
            shape: 'dot',
            text: 'Requests: ' + requests.content + (openRequests.size > 0 ? ' (' + openRequests.size + ' open)' : '')
          })
        }, 500)
      }
    }
    node.updateNodeStatus(true)

    /*
     * Event handling (client-node tells us what to do next)
     */

    const event = 'web:' + node.id
    const handler = function (msg) {
      if (msg._type === 'request') {
        // Error handling if no requestId is set or the id is not known
        if (!msg._request_id) {
          node.error('No request-id set. Does the msg really stem from a page-node?')
          return
        }
        const response = openRequests.get(msg._request_id)
        if (!response) {
          node.error('Request ' + msg._request_id + ' could not be found. Maybe a timeout?')
          return
        }

        // This request is done
        openRequests.delete(msg._request_id)
        node.updateNodeStatus()

        if (msg._return_type === 'send') {
          // Return the file contents
          if (node.contentType === 'path' || node.contentType === 'msg.filename') {
            // Initialize path by replacing placeholders
            let filename = msg.filename
            filename = filename.replace(/{\w*}/g, function (name) {
              return msg.parameters[name.slice(1, -1)] || name
            })
            // Check if the path is valid
            if (filename.indexOf('..') > -1) {
              response.statusMessage = 'Invalid path: ' + filename + '. You must not go up in the directory structure.'
              node.error(response.statusMessage)
              response.status(500).end()
              return
            }
            // Check file existence
            filename = path.join(node.projectHome, 'pekfinger-red', filename)
            try {
              fs.accessSync(filename, fs.constants.F_OK)
            } catch (e) {
              response.statusMessage = "File '" + filename + "' does not exist."
              node.error(response.statusMessage)
              response.status(500).end()
              return
            }
            // Determine mime-type
            let contentType
            if (msg.content_type) {
              contentType = msg.contentType
            } else {
              const mimeType = mimeTypes[path.extname(filename).slice(1).toLowerCase()] || 'text/plain'
              contentType = mimeType
            }
            // Read the file
            const stream = fs.createReadStream(filename)
            stream.on('open', function () {
              response.setHeader('Content-Type', contentType)
              response.setHeader('Caching-Allowed', node.cachingAllowed)
              stream.pipe(response)
            })
            stream.on('error', function () {
              response.setHeader('Content-Type', 'text/plain')
              response.status(500).end('File not found.')
            })
          } else if (node.contentType === 'msg.payload') {
            // Determine mime-type
            let contentType
            if (msg.content_type) {
              contentType = msg.contentType
            } else if (msg.filename) {
              const mimeType = mimeTypes[path.extname(msg.filename).slice(1).toLowerCase()] || 'text/plain'
              contentType = mimeType
            } else {
              contentType = 'text/plain'
            }
            response.setHeader('Content-Type', contentType)
            response.setHeader('Caching-Allowed', node.cachingAllowed)
            // TODO Stream if payload is of type buffer or stream?
            response.send(msg.payload)
          } else {
            node.error("Unsupported web-content type '" + node.contentType + "'.")
          }
        } else if (msg._return_type === 'end') {
          // End immediately
          response.status(msg._status_code || 404).end()
        } else if (msg._return_type === 'redirect') {
          // Redirect
          response.status(msg._status_code || 303).redirect(msg._redirect_to)
        } else {
          node.error("Unknown or unsupported return type '" + msg._return_type + "'.")
        }
      }
    }
    RED.events.on(event, handler)

    // Clean up event handler on close
    node.on('close', function () {
      RED.events.removeListener(event, handler)
    })

    /*
     * Serve content
     */

    node.app.app.get(node.route, function (req, res) {
      // Save request
      const requestId = nextUid('request')
      openRequests.set(requestId, res)
      requests.content += 1
      // Update node-status
      node.updateNodeStatus()
      // The request needs to be cleaned up in case it gets not requested after at the defined timeout (if javascript is disabled or the connection is disrupted)
      setTimeout(function () {
        if (openRequests.has(requestId)) {
          node.warn('Request ' + requestId + ' timed out. It will be forcefully ended now.')
          openRequests
            .get(requestId)
            .status(504)
            .end()
          openRequests.delete(requestId)
          node.updateNodeStatus()
        }
      }, node.app.timeout)
      const msg = {
        _request_id: requestId,
        _type: 'request',
        request: {
          url: req.url,
          parameters: req.params,
          query: req.query
        },
        parameters: { ...req.params, ...req.query },
        content_type: '',
        session_data: {},
        payload: {}
      }

      if (node.contentType === 'path' || node.contentType === 'msg.filename') {
        msg.filename = node.content
      }
      // Is there anything we have to do before we return the content?
      if (node.requestReceivedHook) {
        msg._return_to = node.id
        node.send([msg])
      } else {
        msg._return_type = 'send'
        handler(msg)
      }
    })
  }

  RED.nodes.registerType('web-content', WebContentNode)
}
