const fs = require('fs-extra')
const path = require('path')

module.exports = function (RED) {
  /*
   * Global variables
   */

  // TODO How can we check that noone accesses the files from another project?
  const root = RED.settings.userDir

  /*
   * Helper functions
   */

  function walk (dir) {
    const files = []
    const fileList = fs.readdirSync(dir)
    fileList.forEach(file => {
      const fileInfo = fs.statSync(path.join(dir, file))
      if (fileInfo.isDirectory()) {
        const subFiles = walk(path.join(dir, file))
        subFiles.forEach(subFile => {
          files.push(path.join(file, subFile))
        })
      } else {
        files.push(file)
      }
    })
    return files.map((file) => file.replace('\\', '/').replace('.svelte', ''))
  };

  // Initialize default folder structure if not existing
  function initialize (rootFolder) {
    const pekfingerRedFolderExists = fs.existsSync(path.join(rootFolder))
    if (!pekfingerRedFolderExists) {
      const error = fs.copySync(path.join(__dirname, '..', '..', 'default'), path.join(rootFolder))
      if (error) {
        console.error({ message: 'Could not initialize pekfinger-red folder: ' + error.message, ...error })
      }
    }
  }

  /*
   * List svelte-files
   */

  RED.httpAdmin.get('/pekfinger-red/components', RED.auth.needsPermission('pekfinger-red.read'), function (request, response) {
    const rootPath = request.query.root
    initialize(path.join(root, rootPath))
    const files = walk(path.join(root, rootPath, 'components'))
    response.json(files)
  })

  RED.httpAdmin.get('/pekfinger-red/layouts', RED.auth.needsPermission('pekfinger-red.read'), function (request, response) {
    const rootPath = request.query.root
    initialize(path.join(root, rootPath))
    const files = walk(path.join(root, rootPath, 'layouts'))
    response.json(files)
  })

  /*
   * CRUD operations for svelte-files (components, layouts)
   */

  /* Create */
  // TODO Take care of separator and create subdirectories
  RED.httpAdmin.put('/pekfinger-red/file', RED.auth.needsPermission('pekfinger-red.write'), function (request, response) {
    let filename = request.query.name
    // Check filename existance
    if (!filename) {
      response.statusMessage = "Missing or invalid url parameter 'name': " + filename
      response.status(500).end()
      return
    }
    // Check if the path is valid
    if (filename.indexOf('..') > -1) {
      response.statusMessage = 'Invalid path: ' + filename + '. You must not go up in the directory structure.'
      response.status(500).end()
      return
    }
    // Check file existence
    filename = path.join(root, filename)
    try {
      fs.accessSync(filename, fs.constants.F_OK)
      response.statusMessage = "File '" + request.query.name + "' already exist."
      response.status(500).end()
      return
    } catch (e) {
      // Determine default content
      let content = ''
      if (request.query.name.indexOf('components') > -1) {
        content = `
<!-- Component -->
<script>
    export let client, server, data;
</script>
<style>
</style>
                `.trim()
      } else if (request.query.name.indexOf('layouts') > -1) {
        content = `
<!-- Layout -->
<script>
    export let client, server, data;
    client.title = "New layout";
</script>
<slot>
    <!-- Default slot -->
</slot>
<style>
</style>
                `.trim()
      }
      // Create the file
      fs.writeFileSync(filename, content)
      // Send the content to the client
      response.send(content)
    }
  })

  /* Read */
  RED.httpAdmin.get('/pekfinger-red/file', RED.auth.needsPermission('pekfinger-red.read'), function (request, response) {
    let filename = request.query.name
    // Check filename existance
    if (!filename) {
      response.statusMessage = "Missing or invalid url parameter 'name': " + filename
      response.status(500).end()
      return
    }
    // Check if the path is valid
    if (filename.indexOf('..') > -1) {
      response.statusMessage = 'Invalid path: ' + filename + '. You must not go up in the directory structure.'
      response.status(500).end()
      return
    }
    // Check file existence
    filename = path.join(root, filename)
    try {
      fs.accessSync(filename, fs.constants.F_OK)
    } catch (e) {
      response.statusMessage = "File '" + request.query.name + "' does not exist."
      response.status(500).end()
      return
    }
    // Read the file and send it to the client
    fs.readFile(filename, 'utf8', (error, fileContent) => {
      if (error) {
        response.statusMessage = error.message
        response.status(500).end()
      } else {
        response.send(fileContent)
      }
    })
  })

  /* Update */
  RED.httpAdmin.post('/pekfinger-red/file', RED.auth.needsPermission('pekfinger-red.write'), function (request, response) {
    let filename = request.query.name
    const content = request.body.content
    // Check filename existance
    if (!filename) {
      response.statusMessage = "Missing or invalid url parameter 'name': " + filename
      response.status(500).end()
      return
    }
    // Check content existance
    if (!content) {
      response.statusMessage = "Missing or invalid url parameter 'content': " + content
      response.status(500).end()
      return
    }
    // Check if the path is valid
    if (filename.indexOf('..') > -1) {
      response.statusMessage = 'Invalid path: ' + filename + '. You must not go up in the directory structure.'
      response.status(500).end()
      return
    }
    // Check file existence
    filename = path.join(root, filename)
    try {
      fs.accessSync(filename, fs.constants.F_OK)
    } catch (e) {
      fs.accessSync(filename, fs.constants.F_OK)
      response.statusMessage = "File '" + request.query.name + "' does not exist."
      response.status(500).end()
      return
    }
    // Update file contents
    fs.writeFile(filename, content, (error) => {
      if (error) {
        response.statusMessage = error.message
        response.status(500).end()
      } else {
        response.sendStatus(200)
      }
    })
  })

  /* Delete */
  RED.httpAdmin.delete('/pekfinger-red/file', RED.auth.needsPermission('pekfinger-red.write'), function (request, response) {
    let filename = request.query.name
    // Check filename existance
    if (!filename) {
      response.statusMessage = "Missing or invalid url parameter 'name': " + filename
      response.status(500).end()
      return
    }
    // Check if the path is valid
    if (filename.indexOf('..') > -1) {
      response.statusMessage = 'Invalid path: ' + filename + '. You must not go up in the directory structure.'
      response.status(500).end()
      return
    }
    // Check file existence
    filename = path.join(root, filename)
    try {
      fs.accessSync(filename, fs.constants.F_OK)
    } catch (e) {
      fs.accessSync(filename, fs.constants.F_OK)
      response.statusMessage = "File '" + request.query.name + "' does not exist."
      response.status(500).end()
      return
    }
    // Delete the file
    fs.unlink(filename, (error) => {
      if (error) {
        response.statusMessage = error.message
        response.status(500).end()
      } else {
        response.sendStatus(200)
      }
    })
  })
}
