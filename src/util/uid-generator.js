// A UID generator
let counter = Math.floor((Math.random() * 999999999)) // Start anywhere, so that it cannot be easily guessed
const nextUid = function (prefix) {
  counter += 1
  if (counter === 999999999) counter = 1
  let counterString = counter.toString()
  counterString = new Array(10 - counterString.length).join('0') + counterString
  // The UID will consist of a timestamp, a counter and a random number.
  return (prefix ? prefix + '-' : '') + Date.now() + '-' + counterString + '-' + Math.floor(10000 + (Math.random() * 89999))
}

module.exports = nextUid
