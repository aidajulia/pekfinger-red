module.exports = (description) => {
  // Initialize imports
  let imports = ''
  for (const [tag, file] of Object.entries(description.files)) {
    const filename = file.replace(/\\/g, '\\/')
    imports += `  import ${tag} from '${filename}'\n`
  }
  let dataTemplate = ''
  let defaultTemplate = ''
  let namedSlotsTemplate = ''

  // Process slots
  description.slots.forEach(function (slot) {
    dataTemplate += `  let slot${slot.id}_selector = '${slot.selector}'\n`
    dataTemplate += `  $: slot${slot.id}_props = jsonata(slot${slot.id}_selector).evaluate({msg: {payload: client ? client.data : undefined}})\n`
    let slotTemplate = ''
    slotTemplate += `  {#if slot${slot.id}_props && slot${slot.id}_props.hasOwnProperty("#each") && Array.isArray(slot${slot.id}_props["#each"])}\n`
    slotTemplate += `     {#each slot${slot.id}_props["#each"] as propObj (propObj.id)}\n`
    slotTemplate += `       <${slot.tag} bind:client={client} bind:server={server} bind:data={data} selector={slot${slot.id}_selector} {...propObj}/>\n`
    slotTemplate += '     {/each}\n'
    slotTemplate += '  {:else}\n'
    slotTemplate += `     <${slot.tag} bind:client={client} bind:server={server} bind:data={data} selector={slot${slot.id}_selector} {...slot${slot.id}_props}/>\n`
    slotTemplate += '  {/if}\n'
    if (slot.name === 'default') {
      defaultTemplate += slotTemplate
    } else {
      // The div is a workaround as svelte doesn't allow components within named slots:
      // https://github.com/sveltejs/svelte/issues/1037
      namedSlotsTemplate += `  <div slot='${slot.name}'>\n${slotTemplate}\n  </div>\n`
    }
  })

  // Read head for PWA
  const pwaHead = description.head

  // Prepare template
  let pageTemplate = ''
  pageTemplate += '<script>\n'
  pageTemplate += '  export let client, server, jsonata\n'
  pageTemplate += '  $: data = client ? client.data : undefined\n'
  pageTemplate += `${imports}\n`
  pageTemplate += `${dataTemplate}\n`
  pageTemplate += '</script>\n'
  pageTemplate += '<svelte:head>\n'
  pageTemplate += '  <title>{client.title}</title>\n'
  pageTemplate += `  <link rel="stylesheet" href="/${description.name}/bundle.css">\n`
  pageTemplate += `  ${pwaHead.replace(/\n/g, '\n  ')}\n`
  pageTemplate += '</svelte:head>\n'
  pageTemplate += `<${description.layout} bind:client={client} bind:server={server} bind:data={data}>\n`
  pageTemplate += `${defaultTemplate}\n`
  pageTemplate += `${namedSlotsTemplate}\n`
  pageTemplate += `</${description.layout}>\n`

  return pageTemplate
}
