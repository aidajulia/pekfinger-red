const use = require('./use')
const bundler = require('./bundler')
const jsonata = require('jsonata')
const fs = require('fs-extra')
const path = require('path')

module.exports = (bundle, options) => {
  const bundleClone = { ...bundle }
  bundleClone.ssr = 'let cssSsr = "";\n' + bundleClone.ssr
  bundleClone.ssr = bundleClone.ssr.replace(/styleInject\(([\w$]+)\)/g, 'cssSsr += $1')
  bundleClone.ssr = bundleClone.ssr.replace(/exports\.default = Page_\w+;/, `exports.default = { render: ${options.id}.render, cssSsr }`)
  const compiledTemplate = use(bundleClone.ssr, 'bundleClone.js')
  bundle = {
    timestamp: Date.now()
  }
  // The DOM bundle
  bundleClone.dom = `
    ${bundleClone.dom}
    new ${options.id}.default({
        target: document.querySelector('#pekfinger-red-content'),
        hydrate: true,
        props: {
            client: client,
            server: server,
            jsonata: jsonata
        }
    })
  `
  // The SSR generator
  bundleClone.createSSR = async function (msg) {
    const data = JSON.stringify(msg.payload)
    // Read client script
    const scriptPath = path.join(__dirname, '..', 'communication', 'client.js')
    let clientScript = fs.readFileSync(scriptPath, 'utf-8')
    if (!options.dev) {
      clientScript = await bundler.minify(clientScript, options)
    }
    try {
      const { html, head } = compiledTemplate.render({
        jsonata,
        client: { // dummy
          title: 'App',
          data: msg.payload,
          define () {},
          on () {},
          once () {},
          notify () {},
          getDataFor () {},
          select () {},
          run () {},
          open () {},
          show () {}
        },
        server: { // dummy
          run () {},
          getDataFor () {},
          loadPageFor () {}
        }
      })
      const ssrBundle = `
        <!DOCTYPE html>
        <html${msg.lang ? ' lang="' + msg.lang + '"' : options.defaultLang ? ' lang="' + options.defaultLang + '"' : ''}>
        <head>
            ${head}
        </head>
        <body>
            <div id="pekfinger-red-content">
                ${html}
            </div>
        </body>
        <script src="/socket.io/socket.io.js"></script>
        <script src="/jsonata/jsonata.min.js"></script>
        <script type="module">
            let sessionId = "${msg._session_id}"
            let currentUrl = "${msg.request.url}"
            const version = '${options.version + (options.dev ? ' ' + options.timestamp : '')}'
            export let data = ${data}
            ${clientScript}
            ${options.cachingAllowed ? 'client.dataCache.set(currentUrl, data)' : ''}
        </script>
        </html>
    `
      return ssrBundle
    } catch (error) {
      options.error({
        message: 'Could not render template: ' + error.message,
        stack: error.stack,
        bundle: bundleClone.ssr
      })
    }
  }
  return bundleClone
}
