const fs = require('fs')
const path = require('path')

module.exports = function (options) {
  return {

    name: 'rollup-plugin-pekfinger-red',

    resolveId (source, importedFrom) {
      if (source.startsWith('~/')) {
        options.debug('rollup-plugin-pekfinger-red.resolveId: Resolving ' + source + ' for ' + (importedFrom || 'bundle') + '.')
        const pekfingerRedPath = path.join(options.projectHome, 'pekfinger-red')
        const filename = path.join(pekfingerRedPath, source.substr(2))
        if (fs.existsSync(filename)) {
          options.debug('rollup-plugin-pekfinger-red.resolveId: ' + source + ' found. Resolving it to file ' + filename + '.')
          return {
            id: filename,
            external: false
          }
        }
        options.debug('rollup-plugin-pekfinger-red.resolveId: ' + source + ' not found.')
      }
      return null
    }

  }
}
