/* eslint-disable no-undef */

const PREFIX = 'pekfinger-red-'
const MAIN_CACHE = PREFIX + version
const mainUrls = [
  '/__version__',
  '/manifest.json',
  '/socket.io/socket.io.js',
  '/jsonata/jsonata.min.js'
]
const RESSOURCE_CACHE = PREFIX + version + '-ressources'

self.addEventListener('install', (event) => {
  self.skipWaiting()
  event.waitUntil(
    caches.open(MAIN_CACHE).then((cache) => {
      return cache.addAll(mainUrls)
    })
  )
})

self.addEventListener('activate', (event) => {
  event.waitUntil(
    caches.keys().then((cacheNames) => {
      return Promise.all(
        cacheNames.filter((cacheName) => {
          if (cacheName.startsWith(PREFIX) && cacheName !== MAIN_CACHE && cacheName !== RESSOURCE_CACHE) {
            return true
          } else {
            return false
          }
        }).map((cacheName) => {
          return caches.delete(cacheName)
        })
      )
    })
  )
})

self.addEventListener('fetch', (event) => {
  event.respondWith(caches.match(event.request).then((cacheResponse) => {
    // Cache hit
    if (cacheResponse && event.request.url !== self.registration.scope + '__version__') {
      return cacheResponse
    }
    return fetch(event.request).then((response) => {
      // Check if we received a valid response
      let cachingAllowed = false
      if (response.headers.has('Caching-Allowed')) {
        cachingAllowed = response.headers.get('Caching-Allowed') === 'true'
      }
      if (event.request.method !== 'GET' || event.request.url.startsWith(self.registration.scope + 'socket.io/') || !response || response.status !== 200 || response.type !== 'basic') {
        return response
      }
      // Clone and save the response to the cache
      if (cachingAllowed) {
        const responseToCache = response.clone()
        caches.open(RESSOURCE_CACHE).then((cache) => {
          cache.put(event.request, responseToCache)
        })
      }
      return response
    }).catch((error) => {
      if (cacheResponse) return cacheResponse
      else console.log(error, event.request.url)
    })
  }))
})
