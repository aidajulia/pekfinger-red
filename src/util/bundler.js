const rollup = require('rollup')
const fs = require('fs')
const path = require('path')
const Terser = require('terser')

function getPlugins (options) {
  // Is a custom rollup-configuration present?
  let configPath = path.join(options.projectHome, 'pekfinger-red', 'config')
  if (!fs.existsSync(path.join(configPath, 'rollup-plugins.js'))) {
    configPath = path.join(__dirname, '..', '..', 'default', 'config')
  }
  const configFile = path.join(configPath, 'rollup-plugins.js')

  // Read the configuration
  const rollupConfiguration = require(configFile)

  // Iterate over plugins and add them
  const plugins = []
  rollupConfiguration.forEach((pluginFile) => {
    const plugin = require(path.join(configPath, pluginFile))({ configPath, ...options })
    if (plugin) plugins.push(plugin)
  })
  return plugins
}

const watchMap = new Map()
const optionsMap = new Map()
const pluginMap = new Map()
const bundleMap = new Map()
const callbackMap = new Map()

module.exports = {
  init (options) {
    optionsMap.set(options.id, options)

    // Initialize plugins
    const plugins = {}
    const genericOptions = {
      srcPath: path.join(__dirname, '..'),
      ...options
    }
    plugins.ssr = getPlugins({
      mode: 'ssr',
      ...genericOptions
    })
    plugins.dom = getPlugins({
      mode: 'dom',
      ...genericOptions
    })
    pluginMap.set(options.id, plugins)
  },
  watch (id, updateCallback) {
    const options = optionsMap.get(id)

    options.debug('bundler.watch: Start watching ' + id + '.')
    const plugins = pluginMap.get(id)
    // Add file watcher for SSR
    const ssrPath = path.join(options.buildFolder, 'bundle.ssr.js')
    const ssrWatcher = rollup.watch({
      input: options.file,
      plugins: plugins.ssr,
      inlineDynamicImports: true,
      onwarn (warning) {
        options.warn({
          message: 'Warning while creating bundle.ssr.js.',
          source: options.filename,
          warning
        })
      },
      output: {
        format: 'cjs',
        file: ssrPath,
        exports: 'named',
        sourcemap: options.dev ? 'inline' : false
      }
    })
    ssrWatcher.on('event', event => {
      if (event.code === 'END') {
        options.debug('bundler.watch: SSR bundle for ' + id + ' was built.')
        const code = fs.readFileSync(ssrPath, { encoding: 'UTF-8' })
        if (bundleMap.has(id)) {
          bundleMap.get(id).ssr = code
          // Seems the dom-code is already present
          if (callbackMap.has(id)) {
            callbackMap.get(id).forEach((callback) => callback())
            callbackMap.delete(id)
          }
        } else {
          bundleMap.set(id, { ssr: code })
        }
      } else if (event.code === 'START') {
        options.debug('bundler.watch: Start building SSR bundle for ' + id + '.')
        bundleMap.delete(options.id)
      } else if (event.code === 'ERROR') {
        options.error({ ...event.error })
      }
    })
    // Add file watcher for DOM
    const domPath = path.join(options.buildFolder, 'bundle.dom.js')
    const domWatcher = rollup.watch({
      input: options.file,
      plugins: plugins.dom,
      inlineDynamicImports: true,
      onwarn (warning) {
        options.warn({
          message: 'Warning while creating bundle.dom.js.',
          source: options.filename,
          warning
        })
      },
      output: {
        format: 'iife',
        file: domPath,
        name: options.id,
        exports: 'named',
        sourcemap: options.dev ? 'inline' : false
      }
    })
    domWatcher.on('event', event => {
      if (event.code === 'END') {
        options.debug('bundler.watch: DOM bundle for ' + id + ' was built.')
        const code = fs.readFileSync(domPath, { encoding: 'UTF-8' })
        // Inform clients about update
        updateCallback(code)
        if (bundleMap.has(id)) {
          bundleMap.get(id).dom = code
          // Seems the ssr-code is already present
          if (callbackMap.has(id)) {
            callbackMap.get(id).forEach((callback) => callback())
            callbackMap.delete(id)
          }
        } else {
          bundleMap.set(id, { dom: code })
        }
      } else if (event.code === 'START') {
        options.debug('bundler.watch: Start building DOM bundle for ' + id + '.')
      } else if (event.code === 'ERROR') {
        options.error({ ...event.error })
      }
    })
    watchMap.set(id, { ssrWatcher, domWatcher })
  },
  unwatch (id, options) {
    if (watchMap.has(id)) {
      // Remove current watcher (the nopde got updated)
      options.debug('bundler.watch: Stop watching ' + id + ' due to node-update.')
      const watcher = watchMap.get(id)
      watcher.ssrWatcher.close()
      watcher.domWatcher.close()
    }
  },
  getBundle (id) {
    return new Promise((resolve) => {
      if (bundleMap.has(id) && bundleMap.get(id).ssr && bundleMap.get(id).dom) {
        resolve(bundleMap.get(id))
      } else {
        const callback = () => {
          resolve(bundleMap.get(id))
        }
        if (callbackMap.has(id)) {
          callbackMap.get(id).push(callback)
        } else {
          callbackMap.set(id, [callback])
        }
      }
    })
  },
  async minify (source, options) {
    options.debug('bundler.minify: Minifying source code.')
    return Terser.minify(source).code
  }
}
