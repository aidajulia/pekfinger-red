/**
 * @author Christian Geiger
 */

const express = require('express')
const comServer = require('./communication/server')
const fs = require('fs')
const path = require('path')
const http = require('http')
const https = require('https')
const helmet = require('helmet')
const fileAdmin = require('./util/file-admin')
const mimeTypes = require('./util/mimetypes')

module.exports = function (RED) {
  function WebApp (config) {
    RED.nodes.createNode(this, config)
    const node = this
    node.name = config.name || '<' + config.address + ':' + config.port + '>'
    node.shortName = config.shortName || config.name || 'App'
    node.description = config.description
    node.version = config.version || '1.0'
    node.address = config.address || '0.0.0.0'
    node.port = config.port
    node.tls = config.tls
    node.privateKey = config.privateKey
    node.certificate = config.certificate
    node.baseroute = (config.baseroute[0] === '/' ? '' : '/') + config.baseroute || '/'
    if (!node.baseroute.endsWith('/')) {
      node.baseroute += '/'
    }
    node.timeout = config.timeout
    node.sessionTTL = config.sessionTTL
    node.layout = config.layout
    node.themeColor = config.themeColor || '#ff3e00'
    node.defaultLanguage = config.defaultLanguage
    node.developmentMode = config.developmentMode
    node.scope = config.z === '' ? 'global' : 'flow'
    node.tag = node.name.replace(' ', '_').replace(/\./g, '-').replace(/[^a-zA-Z0-9-_:]/g, '')

    comServer.reset(node) // Reset in case this app gets restarted
    node.app = express()
    node.app.use(helmet()) // Make this app more secure

    // Serve Jsonata
    let jsonataPath = require.resolve('jsonata')
    jsonataPath = jsonataPath.replace('jsonata.js', 'jsonata.min.js')
    const jsonataJs = fs.readFileSync(jsonataPath, 'utf-8')
    node.app.get('/jsonata/jsonata.min.js', function (req, res) {
      res.type('application/javascript').send(jsonataJs)
    })

    // Serve version number
    node.timestamp = Date.now()
    node.app.get('/__version__', function (request, response) {
      response.type('text/plain').send(node.version + (node.developmentMode ? ' ' + node.timestamp : ''))
    })

    // Serve service worker (this function call is a workaround for getting the project directory from the web-page)
    let alreadyServing = false
    node.serveServiceWorker = function (projectHome) {
      if (!alreadyServing) {
        const swPath = path.join(__dirname, 'util', 'serviceworker.js')
        const swJs = fs.readFileSync(swPath, 'utf-8')
        node.app.get('/serviceworker.js', function (req, res) {
          res.type('application/javascript').send("const version = '" + node.version + (node.developmentMode ? ' ' + node.timestamp : '') + "'\n" + swJs)
        })
        alreadyServing = true
      }
    }

    // Serve global imports
    const manifestData = {
      name: node.name,
      short_name: node.shortName,
      description: node.description,
      address: node.address,
      port: node.port,
      baseroute: node.baseroute,
      start_url: node.baseroute,
      scope: node.baseroute,
      theme_color: node.themeColor
    }
    let pwaHead = ''
    node.readHeadForPwa = function (projectHome) {
      if (!pwaHead) {
        const appConfigPath = path.join(projectHome, 'pekfinger-red', 'config', node.tag + '.html')
        const globalConfigPath = path.join(projectHome, 'pekfinger-red', 'config', 'pwa.html')
        if (fs.existsSync(appConfigPath)) {
          pwaHead = fs.readFileSync(appConfigPath, 'utf-8')
        } else if (fs.existsSync(globalConfigPath)) {
          pwaHead = fs.readFileSync(globalConfigPath, 'utf-8')
        }
        pwaHead = pwaHead.replace(/\${(\w*)}/g, (_, name) => manifestData[name] ? manifestData[name] : '')
        const regex = /(?:href|content)\w?=["']\/(.*)["']/g
        let file = regex.exec(pwaHead)
        while (file != null) {
          const filepath = path.join(projectHome, 'pekfinger-red', 'config', file[1])
          if (fs.existsSync(filepath)) {
            const filename = file[1]
            node.app.get('/' + filename, function (request, response) {
              const fileExtension = path.extname(filename).slice(1).toLowerCase()
              const mimeType = mimeTypes[fileExtension] || 'text/plain'
              if (mimeType === 'application/json' || mimeType.startsWith('text')) {
                let configuration = fs.readFileSync(filepath, 'utf-8')
                configuration = configuration.replace(/\${(\w*)}/g, (_, name) => manifestData[name] ? manifestData[name] : '')
                response.type(mimeType).send(configuration)
              } else {
                const stream = fs.createReadStream(filepath)
                stream.on('open', function () {
                  response.setHeader('Content-Type', mimeType)
                  stream.pipe(response)
                })
                stream.on('error', function () {
                  response.setHeader('Content-Type', 'text/plain')
                  response.status(500).end('File not found.')
                })
              }
            })
          }
          file = regex.exec(pwaHead)
        }
      }
      return pwaHead
    }

    function resolvePath (file) {
      if (path.sep === '\\') {
        file = file.replace('/', '\\')
      }
      if (file[0] === '~') {
        file = RED.settings.userDir + file.substring(1)
      }
      return file
    }

    const webServer = node.tls ? https : http
    node.startWebserver = function () {
      if (!node.server) {
        try {
          const options = node.tls ? {
            key: fs.readFileSync(resolvePath(node.privateKey)),
            cert: fs.readFileSync(resolvePath(node.certificate))
          } : {}
          node.server = webServer.createServer(options, node.app)
          node.server.listen(node.port, node.address, function () {
            comServer.init(node)
            RED.log.info("[pekfinger-red] Webserver process for App '" + node.name + "' is listening on " + node.address + ':' + node.port + '.')
          })
          node.server.once('error', function (error) {
            if (error.code === 'EADDRINUSE') {
              node.error('Address ' + node.address + ':' + node.port + ' is already in use.')
            } else {
              node.error(error)
            }
          })
        } catch (e) {
          node.error(e)
        }
      }
    }

    // Prebuild pages if not in development mode
    const pages = []
    this.registerPage = function (page) {
      node.serveServiceWorker(page.projectHome)
      pages.push(page)
      if (!node.server) {
        node.startWebserver()
      }
      page.registerAtGateway()
    }

    node.on('close', function () {
      // Close web-server
      if (webServer) {
        node.server.close(function () {
          RED.log.info("[pekfinger-red] Webserver process for App '" + node.name + "' stopped listening on " + node.address + ':' + node.port + '.')
        })
      }
      // Remove sessions from context
      if (node.scope === 'global') {
        node.context().global.set('web_sessions.' + node.tag, undefined)
      } else {
        node.context().flow.set('web_sessions.' + node.tag, undefined)
      }
    })
  }

  RED.nodes.registerType('web-app', WebApp)

  /*
   * Allow Node-RED client to access the svelte-files
   */
  fileAdmin(RED)
}
