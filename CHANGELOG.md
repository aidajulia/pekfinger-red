> **Attention:** ⚠️ means that a change breaks things. Manual adjustments will be necessary. So be careful before updating. Even data loss might occur.

**Version 0.4.13**

- Removed prerendering as build-process has changed in a way that makes this unnecessary.
- Fixed in-place update of pages on changes.

**Version 0.4.12**

- ⚠️ Small enhancement of build-process. Remember to update your configuration directory (see below).
- Inlined sourcemap for js-bundles (the css-sourcemap gets served directly)
- Fixed error logging in node-RED's debug console
- Bugfix client.js: window.onpopstate threw an error when url was not set

**Version 0.4.11**

- ⚠️ Fixed purgecss handling of svelte-templates. Update configuration directory to enable it for existing projects (see below).

**Version 0.4.10**

- ⚠️ Enabled hydration (head gets extended though - needs further inspection). Please update your configuration manually (see below).
- Updated to current Svelte release
- Minor bugfixes & removal of unnecessary code

**Version 0.4.9**

- Removed 'unsused export' warning
- Improved directory initalization: `pekfinger-red` directory now gets initialized on first read. Thus it is instantly available to the Node-RED client.
- Fixed how the project directory is determined (now during runtime, before during edit-time).
- ⚠️ Changed whole build-process. A manual update of the configuration folder is needed (see below).
- Replaced babel-minify with terser
- CSS gets served now instead of being integrated into the bundle
- Rebuilds are only triggered if a node changed (though an initial full build on startup will always be performed at the moment)
- Integrated purgecss

**Version 0.4.8**

- ⚠️ Fixed rollup integration: All plugins can now be configured through `pekfinger-red/config/rollup-plugins.js` and the corresponding linked files. If you have an old configuration you can copy the files from `node_modules/pekfinger-red/default/config/` (the default is used if `rollup-plugins.js` is not present in your config directory). Please be careful changing things. And be aware that some things might be broken at the moment (testing has not yet finished).
- Changes from version 0.4.7 obsolete

**Version 0.4.7**

- Added possibility to configure postCss through `pekfinger-red/config/postCss.js` (should export the configuration; this gets merged with the default configuration)
- Added possibility to integrate further rollup plugins through `pekfinger-red/config/rollupPlugins.js` (should export an array of plugins)

**Version 0.4.6**

- Bugfix server actions (didn't work - sorry for that)

**Version 0.4.5**

- First candidate to be released to NPM and the Node-RED registry
- Updated README.md and some other information

**Version 0.4.4**

- ⚠️ Removed svelte-material-ui (add it as a dependency of your project if needed)
- Bugfixes for cross-browser support
- Added `client.deferred(callback, delayInMilliseconds)` as better alternative to window.setTimeout
- Bugfix: serviceworker didn't clear cache on update
- Set default interface to 0.0.0.0
- Moved web-page and web-content statistics to flow-context
- Bugfix: session storage
- ⚠️ Renamed sessions to web_sessions in context

**Version 0.4.3**

- Added service worker for full PWA support
- Added offline support
- Added doctype
- Added language attribute
- Added allow-caching to web-content & corresponding client-server-handling
- Bugfixes, partially better exception handling

**Version 0.4.2**

- Pages get prerendered on startup (sequentially for each App) if not in development mode
- The Webserver-process is not started until the prerendering is done (if it is done)
- Removed minification of bundles as it resulted in an error
- Extended logging in dev mode
- Fixed bug regarding doubled load page requests
- Enhanced web-node's status
- Bugfix: socket might not have been defined in communication/server.js
- Bugfix: slots had wrong properties when used multiple times
- Added scroll-to-top on page-change
- Added possibility to react on page-switches (e.g. for animation)

**Version 0.4.1**

- Updated node-modules to current version
- Implemented caching for Svelte-components

**Version 0.4.0**

- Added dynamic configuration
- Added JSON-support to bundler
- Added parameters to redirect
- ⚠️ Changed page-handling

**Version 0.3.9**

- ⚠️ New folder structure: The theme file is now located in the config directory. Furthermore, icons have a subfolder of their own now.
- Added project's node_modules folder to search path

**Version 0.3.8**

- Bugfixes
- End wil not warn anymore in case of actions
- Actions can now result in a send meaning that the client-data gets updated. But use with care: Svelte won't recognize the changed data so you'll have to refresh manually.
- Bugfix: 'Address in use'-error does not crash node-red anymore

**Version 0.3.7**

- Introduced variables within manifest (and other text-files) using `${name}` syntax
- Introduced shortname, description and theme-color to App
- Fixed bug concerning lower case component names (they get automatically capitalized now)
- Started with the documentation of the nodes

**Version 0.3.6**

- Added default pekfinger-red directory
- Bugfix theme-file resolution
- Added basic support for mainfest and favicon

**Version 0.3.5**

- Actions defined through `client.define` are now scoped to the current page and removed on page change.
- Global actions can be defined using `client.defineGlobal`. These can only be defined once and will be overwritten when defined a second time.

**Version 0.3.4**

- Fixed SSR of SMUI components (styles were not rendered server-side)
- Added workaround for postcss / node-sass bug

**Version 0.3.3**

- Fixed resolving resources from modules
- Added support for SASS
- Added Svelte Material UI (https://sveltematerialui.com/)

**Version 0.3.2**

- Added web-content-node
- Added support for authenticating against admin-API (file-admin)

**Version 0.3.1**

- ⚠️ Renamed project to pekfinger-red
- ⚠️ Renamed folder containing components and layouts from web-red to pekfinger-red

**Version 0.3.0**

- Actions from other pages can now be called through `server.run("myAction", "myData", {url: "page-url"})}` whereby the page-URL defines the page the
  action shall be triggered on. This allows to define global actions within a global-page (with just actions and no content) like e.g. `addToCart`.
  Any client-action that gets triggered from the server-action is executed on the currently shown page and thus should be defined globally.
- Data for another page can be accessed through `client.getDataFor("page-url")`
- Added event handling: The client can react on events like `server.setData`, `server.sessionInvalid` or `history.back` by registering an
  event handler with `client.on('event', (event) => {console.log(event.data)})`. Some events can be stopped by returning false from within
  the event handler. A documentation on this should follow some day.
- ⚠️ Cleaned up variable names (camelCase for javascript and snake_case for json). This means that all web-client-node need to be updated (better now 
  than later ;). Reasons for snake_case for json is better readability when they are shown on the Node-RED UI and differentiation within the code.
- The session data is now made available within the context data

**Version 0.2.7**

- ⚠️ Introduced default-timeout: This must be added to existing web-app-nodes
- Made page-caching configurable
- Added node-status to web-page and web-client (blinks on activity, shows count)
- Added auto-reload on deploy
- Added session data to all node-outputs (msg.session_data). It will automatically be updated if still present on msg when reaching the client-node.
- Introduced development mode for disabling minification (other features might follow)
- Bugfixes
- Added StandardJS
- Added support for browser history

**Version 0.2.6**

- Added support for preloading of pages (through `server.loadPageFor(url)`)
- Added support for preloading of data (through `server.getDataFor(url)`)
- Redirect to pages using `client.open(url[, data])`)
- Pages are now cached (*not* yet configurable)
- Auto-reload of current page in case of expired session

**Version 0.2.5**

- You can access the data through `client.data` now. `data` is kept as a shorthand
- ⚠️ `server.call` is replaced by `server.run` to unify actions run on the client and on the server